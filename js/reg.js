
var header=["Username","Name","Email","Password","Age"]
var UP_ARROW="&#x2191;"
var DOWN_ARROW="&#x2193;"
var UP_DOWN_ARROW="&#x2195;"

var regListRef=firebase.database().ref("users/")


//loadList(regList)
regListRef.on("value",function(snapshot){
    loadList(snapshot)
})
/*regListRef.on("child_added",function(snapshot){
    loadList(snapshot)
})*/
/*regListRef.orderByChild("Age").equalTo("222").on("child_added",function(snapshot){
    console.log(snapshot.val()["Username"])
})*/
console.log(config.databaseURL)

function createXHR(reqType,url){
    var xhr=new XMLHttpRequest()
    xhr.open(reqType,url)
    xhr.setRequestHeader('Content-Type','json')
    return xhr
}


function loadList(snapshot){
    var table=document.getElementById("regList")
      
    table.innerHTML=""
    
    var tr=document.createElement("tr")
    for(var i=0;i<header.length;i++){
        var th=document.createElement("th")
        var s=""
        if(header[i]=="Name"||header[i]=="Age"){
            s=header[i]+UP_DOWN_ARROW
            th.setAttribute("name",header[i])
            th.setAttribute("onclick","sortColumn(this)")
        }
        else{
            s=header[i]
        }
        th.innerHTML=s
        tr.appendChild(th)
    }
    table.appendChild(tr)
    
    /*for(var i=0;i<regList.length;i++){
        var tr=document.createElement("tr")
        for(var j=0;j<header.length;j++){
            var td=document.createElement("td")
            td.innerHTML=regList[i][header[j]]
            tr.appendChild(td)
        }
        table.appendChild(tr)
    }*/
    
    snapshot.forEach(function(record){
        var tr=document.createElement("tr")
        for(var j=0;j<header.length;j++){
            var td=document.createElement("td")
            td.innerHTML=record.val()[header[j]]
            tr.appendChild(td)
        }
        table.appendChild(tr)
    })
    
    //console.log(regList)
}


function newUser(){
    var username=document.getElementById("username").value
    var name=document.getElementById("name").value
    var email=document.getElementById("email").value
    var password=document.getElementById("password").value
    var age=parseInt(document.getElementById("age").value)
    if(hasUser(username)){
        console.log(username+" existed")
    }
    else{
        var user={
            "Username":username,
            "Name":name,
            "Email":email,
            "Password":password,
            "Age":age
        }
        
        /*regListRef.push(user).catch(function(error){
            console.error("Error during writing user data",error);
      });*/
      var xhr=createXHR('POST',config.databaseURL+'/users.json')
      xhr.send(JSON.stringify(user))
    }
    
    //loadList()
    
}



function getAgeAvg(){
    var totalAge=0
    var count=0
    regListRef.once('value',function(snapshot){
        snapshot.forEach(function(record){
            totalAge+=record.val()["Age"]
            count++
        })
    })
    /*for(var i=0;i<regList.length;i++){
        totalAge += parseFloat(regList[i]["Age"])
    }*/
    var avgAge=totalAge/count
    alert("Average Age:"+avgAge)

}


function hasUser(username){
    /*return regList.filter(function(user){
        return user["Username"]==username
    }).length > 0*/
    var result=false
    regListRef.orderByChild("Username").equalTo(username).on("child_added",function(snapshot){
        result=true
    })
    return result
}

function sortColumn(th){
    var column=th.getAttribute("name")
    /*regList.sort(function(a,b){
        if(a[column]>b[column]){
            return 1
        }
        else if(a[column]<b[column]){
            return -1
        }
        else{
            return 0
        }
    })*/
    var tempRef=regListRef.orderByChild(column)
    tempRef.once('value',function(snapshot){
        loadList(snapshot)
    })
    console.log(column+" sorted")
    //loadList(regList)
}

function filterAge(){
    var minAge=parseInt(document.getElementById("minAge").value)
    var maxAge=parseInt(document.getElementById("maxAge").value)
    /*var tempList=regList
    if(minAge!="" && maxAge!=""){
        tempList=tempList.filter(function(user) {
            return user["Age"]>=minAge && user["Age"]<=maxAge
        })
    }*/
    var tempRef=regListRef
    console.log(minAge+" "+maxAge)
    if(!isNaN(minAge) && !isNaN(maxAge)){
        tempRef=tempRef.orderByChild("Age").startAt(minAge).endAt(maxAge)
    }
    tempRef.once('value',function(snapshot){
        loadList(snapshot)
    })
}
